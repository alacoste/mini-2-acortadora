from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Shortener
from django.template import loader
from django.core.management.commands.runserver import Command

# Create your views here.

@csrf_exempt
def index(request):
    new_shortened = ''
    error = ''
    if request.method == "POST":
        url2cut, short, error = get_atributes(request)
        if not url2cut: # este caso sólo ocurre si se reciben peticiones erroneas mediante curl y RESTclient
            template, context = set_template(request, new_shortened, error)
            return HttpResponse(template.render(context, request))
        shorted = "http://localhost:" + str(Command.default_port) + "/"
        if url_exists(request, url2cut) and not shorted_exists(request, shorted + short):
            new_shortened = switch_shorted(request, short, shorted, url2cut)
        elif short:
            shorted += short
            try:
                if url_exists(request, url2cut): # con este if se evita que se cree un nuevo enlace si ya existe uno
                    to_del = Shortener.objects.get(url=url2cut)
                    to_del.delete()
                new_shortened = Shortener.objects.get(shorted=shorted)
                new_shortened.url = url2cut
            except Shortener.DoesNotExist:
                new_shortened = Shortener(shorted=shorted, url=url2cut)
        else:
            shorted += str(set_counter(request))
            new_shortened = Shortener(shorted=shorted, url=url2cut)
        new_shortened.save()
    template, context = set_template(request, new_shortened, error)
    return HttpResponse(template.render(context, request))

@csrf_exempt
def redirect(request, key):
    shorted = "http://localhost:" + str(Command.default_port) + "/" + key
    try:
        url = Shortener.objects.get(shorted=shorted)
    except Shortener.DoesNotExist:
        raise Http404("URL not found")
    return HttpResponseRedirect(url.url)


def switch_shorted(request, short, shorted, url2cut):
    new_shortened = Shortener.objects.get(url=url2cut)
    if short:
        new_shortened.shorted = shorted + short
    else:
        new_shortened.shorted = shorted + str(set_counter(request))
    new_shortened.save()
    return new_shortened

def shorted_exists(request, shorted):
    try:
        Shortener.objects.get(shorted=shorted)
        return True
    except Shortener.DoesNotExist:
        return False
def url_exists(request, url):
    try:
        Shortener.objects.get(url=url)
        return True
    except Shortener.DoesNotExist:
        return False


def set_counter(request):
    counter = 0
    shortened = Shortener.objects.all()
    for url in shortened:
        try:
            auxcounter = int(url.shorted.split('/')[-1])
        except ValueError:
            auxcounter = 0
        if auxcounter > counter:
            counter = auxcounter
    counter += 1
    return counter

def set_template(request, current, error):
    list_shorted = Shortener.objects.all()
    template = loader.get_template('index.html')
    context = {
        'shortened': list_shorted,
        'current': current,
        'error': error
    }
    return template, context

def splitqs(request):
    parts = request.body.decode('utf-8').split('&')
    result = []
    for part in parts:
        key, value = part.split('=')
        result.append(key)
        result.append(value)
    return result

def get_atributes(request):
    """
    El siguiente if es para que el servidor funcione con la extensión RESTclient y Curl correctamente.
    En el funcionamiento normal de un cliente corriente usando la pag web, nunca saltaría al else(no deja urls vacías).
    Si salta, es porque alguien está haciendo peticiones mediante Curl o RESTclient
    """
    url = request.POST.get('url') # si esto devuelve None, es porque se solicitó con RESTclient o Curl
    if url:
        shorted = request.POST.get('short')
    else: # si url es None, se procesa diferente, ya no vale el request.POST.get('url')
        try:
            splittedqs = splitqs(request)
            if splittedqs[0] != 'url' or splittedqs[2] != 'short':
                raise IndexError
            url = splittedqs[1]
            shorted = splittedqs[3]
            if not url:
                return None, None, 'Debes ingresar una URL'
        except IndexError:
            return None, None, 'Usando Curl o RESTclient solo se aceptan querystrings del formato url=url&short=short'
        except ValueError:
            return None, None, 'Usando Curl o RESTclient solo se aceptan querystrings del formato url=url&short=short'
    if not url.startswith('http://') and not url.startswith('https://'):
        url = "https://" + url
    return url, shorted, None
