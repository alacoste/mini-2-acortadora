from django.db import models

# Create your models here.

class Shortener(models.Model):
    shorted = models.CharField(max_length=100)
    url = models.TextField()

    def __str__(self):
        return self.shorted
